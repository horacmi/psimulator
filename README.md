# Psimulator

Psimulator je program, který umožňuje vytvoření virtuální počítačové sítě a následně simulovat a analyzovat datový traffic na této síti.
Program obsahuje uživatelské rozhraní pro snadnou tvorbu
a úpravu schémat počítačových sítí. Backend aplikace umožňuje simulovat
počítače s operačním systémem Linux nebo Cisco IOS a síťovou komunikaci
mezi těmito počítači na linkové, síťové a transportní vrstvě.

##  Systémové požadavky

 * **Java Runtime Environment (JRE) verze 7+**
 * **Telnet klient**

      Simulátor obsahuje vestavěný telnet klient. Pokud by ale nevyhovoval, je možné použít
      např. Putty (http://www.putty.org/) nebo systémový příkaz telnet

 *  **Knihovna libpcap (winpcap na OS Windows)**

      Pouze pro potřebu propojování virtuální a reálné sítě. Instalační balíčky jsou součástí projektového archívu.

##  Frontend rozhraní

Pro jednoduchou práci se simulátorem je možné použít uživatelského rozhraní. To se skládá ze dvou
částí. První z nich je editor, pomocí kterého se dají vytvářet nebo upravovat schémata virtuální
počítačové sítě. Druhou částí je potom simulátor, ve kterém dojde ke spuštění virtuální sítě a je
umožněn telnet přístup k jednotlivým virtuálním strojům. V simulačním režimu je také možné
zachytávat a číst komunikaci na vytvořené síti.
Uživatelské rozhraní obsahuje vlastní detailní nápovědu nad rámec tohoto manuálu. Pro přístup k ní
slouží nabídka Nápověda → Nápověda. Možné je využít i klávesové zkratky **F1**

![psimulator.png](https://bitbucket.org/repo/5qn7kzk/images/1128471580-psimulator.png)

### Editor

Editor slouží k vytváření nebo editaci modelů virtuálních sítí a následné uložení těchto modelů do
XML souborů.
Okno editoru obsahuje tyto části:

 * **Hlavní panel**

    Obsahuje přístup k vytváření, načítání a ukládání konfigurací, nastavení editoru a spuštění
    simulace virtuální sítě.

 * **Panel komponent**

    Umožňuje výběr jednotlivých komponent virtuální sítě. Kliknutí pravým tlačítkem myši na
    komponentu otevře výběr typu komponenty (linuxový, CISCO router). Levým tlačítkem
    myši je poté možné označit komponentu pro přenos na pracovní plochu editoru.

 * **Pracovní plocha editoru**

    Plocha pro konstrukci schématu virtuální sítě.
    Po kliknutí pravým tlačítkem myši na virtuální zařízení a výběru možnosti Vlastnosti se
    otevře okno pro konfiguraci síťových rozhraní na tomto zařízení. Rozhraní je možné
    nastavit jako statické nebo dynamické.
    Pro statické je třeba do políčka IP Adresa / maska zadat IPv4 adresu ve tvaru:
    IPv4 adresa / počet bitů masky (např. 10.0.0.1/8).
    Pro dynamickou konfiguraci rozhraní se políčko IP Adresa / maska nechá prázdné.

### Simulátor

![psimulator.png](https://bitbucket.org/repo/5qn7kzk/images/1353396417-psimulator.png)

Pro zapnutí simulačního režimu grafického rozhraní slouží tlačítko *Přepnout do simulačního režimu*
na hlavním panelu.
Po spuštění simulace v editorovém režimu se simulátor přepne do režimu simulačního. Při tom
dojde ke spuštění jednotlivých virtuálních zařízení.
V pravé části okna editoru se zobrazí panel simulace (zobrazený výše), ve kterém je možné
zachytávat komunikaci, která na síti probíhá. K zapnutí tohoto zachytávání slouží tlačítko
Zachytávat v prostřední části panelu. Při probíhající komunikaci se poté v okně Seznam událostí
budou objevovat pakety, proudící v síti. Po kliknutí na jednotlivé pakety je poté v dolní části v
panelu Detaily vybraného paketu možné jednotlivé pakety prozkoumat blíže.

## Backend shell

Linuxová a CISCO zařízení obsahují shell sloužící k jejich ovládání. Pokud je simulace zapnutá, je
možné se k tomuto shellu připojit kliknutím pravým tlačítkem myši na vybrané zařízení a výběrem
možnosti *Otevřít telnet*.
Pro bližší informace o seznamu implementovaných příkazů zadejte příkaz **help**.
Na linuxových zařízeních je možné pro vice detailů o jednotlivých příkazech zadat příkaz s
přepínačem **-h**

### Souborový systém

Linuxová zařízení obsahují jednoduchý souborový systém, který umožňuje snazší a perzistentní
možnost konfigurace.
K tvorbě a úpravě souborů byl vytvořen textový editor spustitelný příkazem **editor**

## Propojení s reálnou sítí

Do topologie se umístí prvek *Reálný počítač* a v jeho vlastnostech je třeba vybrat výstupní interface,
který je dostupný na hostitelském počítači.
Po spuštění simulace je pomocí příkazu **rnetconn** svázat switchport odpojídajícího virtuálního
zařízení reprezentující reálný počítač se skutečným rozhraním na hostitelském stroji . Pro nápovědu
k tomuto příkazu slouží příkaz **rnetconn help**. Je nutné, aby tento virtuální interface byl
nakonfigurovaný stejně jako ten reálný.